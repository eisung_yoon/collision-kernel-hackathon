DFLAGS_band=-DELLIPTICS_NEW -DSOLVERLU 
DFLAGS_petsc=-DELLIPTICS_NEW -USOLVERLU 
DFLAGS=$(DFLAGS_band)


CMP=ftn $(DFLAGS)  -I.  -ffree-form
OPT_g=   -g  -fbounds-check
OPT_omp=   -O  -fopenmp
OPT=$(OPT_omp)

CPP=cpp -E $(DFLAGS)  
# CPP=fpp  $(DFLAGS) -P 




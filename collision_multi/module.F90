module col_sa_fpl_module
  implicit none
  !PHYSICS & MATH CONST
  real(kind=8), parameter :: sml_prot_mass = 1.6720D-27  !! proton mass (MKS)       !from XGC1
  real(kind=8), parameter :: sml_e_charge = 1.6022D-19   !! electron charge (MKS)   !from XGC1
  real(kind=8), parameter :: sml_pi = 3.1415926535897932 !! from XGC1 
  logical :: col_f_multisp_on
  ! <USER INPUT FOR DOMAIN OF VELOCITY SPACE>
  ! - # of mesh
  integer :: col_f_nvz = 64    ! Note that domain of parallel v:  [-vi_par, vi_par]
  integer :: col_f_nvr = 32
  integer :: col_f_ntotal_v =   2048 ! col_f_nvz*col_f_nvr
  integer :: LU_n = 2048             ! col_f_ntotal_v
  integer :: LU_nnz = 17860 ! 4*4 + 6*2*(col_f_nvr-2)+6*2*(col_f_nvz-2)+9*(col_f_nvr-2)*(col_f_nvz-2)
  integer, parameter :: LU_nrhs = 1
  integer :: LU_ldb = 2048  ! LU_n

  integer :: ncells = 16
  real(kind=8) :: col_f_dt = 1.0d-2
  real(kind=8) :: col_f_t_tot = 1.0d0
  real(kind=8) :: vi_par_beg, vi_par_delta, vi_perp_delta, vthi_par, vthi_perp
  real(kind=8) :: ve_par_beg, ve_par_delta, ve_perp_delta, vthe_par, vthe_perp
  real(kind=8), allocatable, dimension(:) :: voli, vole
  integer, allocatable, dimension(:,:) :: index_map_LU
  integer, allocatable, dimension(:) :: LU_cvalues, LU_rowindx, LU_colptr

  !!integer :: col_f_nthreads = 8
  integer :: col_f_nthreads = 1  !For teseting (w/o NESTED OMP)

  type col_f_core_type
      integer :: spi
      real (kind=8) :: numeric_vth2, numeric_T, numeric_Teq, numeric_vtheq2
      real (kind=8) :: mass, mesh_dr, mesh_dz, dens, ens, mom  !dr : dv_perp, dz: dv_para
      real (kind=8), allocatable, dimension(:) :: mesh_r, mesh_r_half, mesh_z, mesh_z_half, &
                                                  local_center_volume, vol
      real (kind=8), allocatable, dimension(:,:) :: delta_r, delta_z
  end type col_f_core_type

#ifndef SOLVERLU
#include <finclude/petscsysdef.h>
#include <finclude/petscsnesdef.h>
#include <finclude/petscdmdef.h>


  Mat ::  col_f_mat
  Vec ::  col_f_vecb
  Vec ::  col_f_vecx
  KSP ::  col_f_ksp
#endif

  contains

  subroutine init_col_sa_fpl_module
    implicit none

    integer, parameter :: indev = 14
    character*80 :: filename  = 'input.txt'

    integer :: istatus

    namelist /col_param/ ncells, col_f_nvr,col_f_nvz,col_f_dt,col_f_t_tot,col_f_multisp_on


    ncells = 16
    col_f_nvz = 64
    col_f_nvr = 32
    col_f_dt = 1.0d-2
    col_f_t_tot = 1.0d0
    col_f_multisp_on=.true.

    open(indev,file=trim(filename),access='sequential',form='formatted',   &
        status='old',iostat=istatus)
    if (istatus.eq.0) then
      rewind(indev)
      read(indev,nml=col_param)
      close(indev)
    endif
   write(*,9010) ncells,col_f_nvr,col_f_nvz,col_f_dt,col_f_t_tot
 9010 format(' ncells= ',i6,' col_f_nvr= ',i6,' col_f_nvz= ',i6,/,       &
             ' col_f_dt= ',1pe16.6,' col_f_t_tot= ',1pe16.6)


    col_f_ntotal_v = col_f_nvz*col_f_nvr
    LU_n = col_f_ntotal_v
    LU_nnz = 4*4 + 6*2*(col_f_nvr-2)+6*2*(col_f_nvz-2)+9*(col_f_nvr-2)*(col_f_nvz-2)
    LU_ldb = LU_n


    allocate( voli(col_f_nvr) )
    if (col_f_multisp_on) then
      allocate( vole(col_f_nvr) )
    endif

    return
  end subroutine init_col_sa_fpl_module


end module col_sa_fpl_module

subroutine make_initial_pdf_bimaxwell(dens, v_par0, dv_par, dv_perp, vth_par, vth_perp, pdf)
   use col_sa_fpl_module, only: sml_pi, col_f_nvr, col_f_nvz
   implicit none
   real (kind=8) :: dens
   real (kind=8) :: v_par0, dv_par, dv_perp, vth_par, vth_perp
   real (kind=8), dimension(col_f_nvr,col_f_nvz) :: pdf
   integer :: i, j
   real (kind=8) :: vperp, vpar
   real (kind=8) :: maxwell_coeff

   maxwell_coeff = dens/((2D0*sml_pi)**1.5D0*vth_par*vth_perp**2)
   do i=1, col_f_nvz
      vpar = v_par0 + dv_par * (i-1)
      do j=1, col_f_nvr
         vperp = dv_perp * (j-1) 
         pdf(j,i) = maxwell_coeff*exp(-0.5D0*( vperp*vperp/(vth_perp*vth_perp) + vpar*vpar/(vth_par*vth_par)))
      enddo
   enddo
end subroutine make_initial_pdf_bimaxwell


subroutine col_f_lambda_gamma(den,ti_ev,te_ev, massi, masse, gammac)
  use col_sa_fpl_module
  implicit none

  real (8), intent(in) :: den, ti_ev, te_ev, massi, masse
  real (8), intent(out) :: gammac(4)
  real (8) :: lambda(3)

  ! NRL Plasma Formulary 2011 version, p.34
  !(c) Mixed ion-ion collisions (here, same species ion-ion collisions)
  lambda(1) = 2.3D1 - log(sqrt( 2D0*den*1D-6/ti_ev)/ti_ev)

  ! i-i
  gammac(1) = (sml_e_charge**4) * lambda(1) / ( massi*((8.8542D-12)**2) * 8D0 * sml_pi )

  if (col_f_multisp_on) then
    !(b) Electron-ion (and ion-electron) collisions
    if(ti_ev * masse/massi .lt. te_ev) then
       if(te_ev .lt. 1D1) then
          lambda(2) = 2.3D1 - log(sqrt(den*1D-6/(te_ev**3)))
       else
          lambda(2) = 2.4D1 - log(sqrt(den*1D-6)/te_ev)
       endif
    else
       lambda(2) = 3D1 - log(sqrt(den*1D-6/(ti_ev**3)))
    endif


    !(a) Thermal electron-electron collisions
    lambda(3) = 2.35D1 - log(sqrt(den*1D-6)*(te_ev**(-1.25D0))) - sqrt(abs(1D-5+((log(te_ev) -2D0)**2)/16D0))

    ! i-e
    gammac(2) = (sml_e_charge**4) * lambda(2) / ( massi*((8.8542D-12)**2) * 8D0 * sml_pi )
    ! e-i
    gammac(3) = (sml_e_charge**4) * lambda(2) / ( masse*((8.8542D-12)**2) * 8D0 * sml_pi )
    ! e-e
    gammac(4) = (sml_e_charge**4) * lambda(3) / ( masse*((8.8542D-12)**2) * 8D0 * sml_pi )
  endif

end subroutine col_f_lambda_gamma

subroutine col_f_setup
  use col_sa_fpl_module
  implicit none
  integer :: i, j
  integer :: mat_pos_rel(9), mat_pos_rel_indx(9), LU_i_arr(9)
  integer :: elem_n, mat_pos, LU_i, LU_j, incr_LU
  integer, allocatable, dimension(:) :: LU_colptr_num

!$omp critical (alloc1)
  allocate(LU_rowindx(LU_nnz), LU_cvalues(LU_n), LU_colptr(LU_n+1), index_map_LU(9,LU_n))       !global
  allocate(LU_colptr_num(LU_n))    !local
!$omp end critical (alloc1)

  LU_colptr_num = 0   ! number of elements in each column, local
  LU_rowindx = 0         ! global

  !below is time independent. Move to init_col in module
  LU_colptr(1) = 1
  do i=1,LU_n
      !for colptr
      LU_i = (i-1) / col_f_nvr+1
      LU_j = mod(i-1, col_f_nvr)+1
      if( (LU_i .eq. 1) .or. (LU_i .eq. col_f_nvz) ) then
          if( (LU_j .eq. 1) .or. (LU_j .eq. col_f_nvr) ) then
              incr_LU = 4
          else
              incr_LU = 6
          endif
      else
          if( (LU_j .eq. 1) .or. (LU_j .eq. col_f_nvr) ) then
              incr_LU = 6
          else
              incr_LU = 9
          endif
      endif
      LU_colptr(i+1) = LU_colptr(i)+incr_LU
  enddo

  !===============
  !  3--6--9
  !  |  |  |
  !  2--5--8
  !  |  |  |
  !  1--4--7
  !==============
  mat_pos_rel=(/-col_f_nvr-1,-col_f_nvr, -col_f_nvr+1, -1, 0, 1, col_f_nvr-1, col_f_nvr, col_f_nvr+1/)
  index_map_LU = 0
  do i=1,col_f_nvz
      do j=1, col_f_nvr
          mat_pos = j+(i-1)*col_f_nvr

          ! INDEXING
          if(i .eq. 1) then
              if(j .eq. 1) then
                  !(J,I)=(1,1)
                  elem_n = 4
                  mat_pos_rel_indx(1:elem_n) =(/5,6,8,9/)
               elseif (j .eq. col_f_nvr) then
                  !(J,I)=(Nr,1)
                  elem_n = 4
                  mat_pos_rel_indx(1:elem_n)=(/4,5,7,8/)
               else
                  !(J,I)=(:,1)
                  elem_n = 6
                  mat_pos_rel_indx(1:elem_n)=(/4,5,6,7,8,9/)
               endif
           elseif(i .eq. col_f_nvz) then
               if(j .eq. 1) then
                  !(J,I)=(1,mesh_Nz)
                  elem_n = 4
                  mat_pos_rel_indx(1:elem_n)=(/2,3,5,6/)
               elseif (j .eq. col_f_nvr) then
                  !(J,I)=(Nr,mesh_Nz)
                  elem_n = 4
                  mat_pos_rel_indx(1:elem_n)=(/1,2,4,5/)
               else
                  !(J,I)=(:,mesh_Nz)
                  elem_n = 6
                  mat_pos_rel_indx(1:elem_n)=(/1,2,3,4,5,6/)
               endif
           else
               if(j .eq. 1) then
                  !(J,I) = (1,:)
                  elem_n = 6
                  mat_pos_rel_indx(1:elem_n)=(/2,3,5,6,8,9/)
               elseif(j .eq. col_f_nvr) then
                  !(J,I) = (mesh_Nr,:)
                  elem_n = 6
                  mat_pos_rel_indx(1:elem_n)=(/1,2,4,5,7,8/)
               else
                  !(J,I) = (:,:)
                  elem_n = 9
                  mat_pos_rel_indx(1:elem_n)=(/1,2,3,4,5,6,7,8,9/)
               endif
           endif
           LU_i_arr(1:elem_n) = mat_pos+mat_pos_rel(mat_pos_rel_indx(1:elem_n))  ! I need to change LU_i to array
           index_map_LU(mat_pos_rel_indx(1:elem_n),mat_pos) = LU_colptr(LU_i_arr(1:elem_n))+LU_colptr_num(LU_i_arr(1:elem_n))
           LU_colptr_num(LU_i_arr(1:elem_n)) = LU_colptr_num(LU_i_arr(1:elem_n))+1
           LU_rowindx(index_map_LU(mat_pos_rel_indx(1:elem_n),mat_pos)) = mat_pos


           LU_cvalues(mat_pos) = index_map_LU(5, mat_pos)  !For implicit time marching
      enddo
  enddo
!$omp critical (alloc1)
  deallocate(LU_colptr_num)
!$omp end critical (alloc1)


#ifndef SOLVERLU
  call col_f_petsc_initialize
#endif
           

#ifndef SOLVERLU
  contains
    subroutine col_f_petsc_initialize
      use col_sa_fpl_module
      implicit none
      integer :: j, indx

#include <finclude/petscsys.h>
#include <finclude/petscvec.h>
#include <finclude/petscmat.h>
#include <finclude/petscksp.h>
#include <finclude/petscpc.h>

       PetscErrorCode:: ierr
       PetscInt :: LU_n64
       PetscInt, parameter :: pint_13=13, ione=1
       !Mat col_f_mat
       !Vec col_f_vecb
       !Vec col_f_vecx
       !KSP col_f_ksp

       LU_n64=LU_n

      !PETSc for collision-f
      ! code addopted from ksp/examples/tutorials/ex13f90.F.html
      !Using PCLU
      call MatCreateSeqAIJ(PETSC_COMM_SELF,LU_n64, LU_n64, pint_13, PETSC_NULL_INTEGER, col_f_mat, ierr) ! 13 width - 9 is mimimum
      call VecCreateSeqWithArray(PETSC_COMM_SELF,ione,LU_n64, PETSC_NULL_SCALAR,col_f_vecb,ierr)
      call VecDuplicate(col_f_vecb,col_f_vecx,ierr)
      call KSPCreate(PETSC_COMM_SELF,col_f_ksp,ierr)

!rh LU_colindx is never used anywhere in the code.
!rh !$omp critical (alloc1)
!rh       allocate(LU_colindx(LU_nnz))
!rh !$omp end critical (alloc1)

!rh       do j=1, LU_n
!rh          do indx=LU_colptr(j), LU_colptr(j+1)-1
!rh             LU_colindx(indx)=j - 1
!rh          enddo
!rh       enddo

    end subroutine col_f_petsc_initialize
#endif
end subroutine col_f_setup


module unload PrgEnv-pgi
module unload PrgEnv-cray
module unload PrgEnv-intel
module unload PrgEnv-gnu

module unload cray-libsci
module unload cray-petsc
module unload cudatoolkit
module unload craype-accel-nvidia35
module unload cray-libsci_acc


module load PrgEnv-pgi
module load cray-petsc
module load cudatoolkit

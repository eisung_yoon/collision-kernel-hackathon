DFLAGS_band=-DELLIPTICS_NEW -DSOLVERLU 
DFLAGS_petsc=-DELLIPTICS_NEW -USOLVERLU 
DFLAGS=$(DFLAGS_band) -UUSE_VECTOR


CMP=ftn $(DFLAGS) 

OPT_O=  -Mfree -fast -O3 -Minfo=all  -I. -Kieee -Mstack_arrays
OPT_g=  -Mfree -Mbounds -g  -I. -Kieee -Mstack_arrays -Mcuda=lineinfo #-Mprof=ccff
OPT_omp=  -Mfree -mp -fast  -I.  -Mstack_arrays
OPT_acc = -Mfree -fast -mp  -acc -ta=tesla:nollvm,keepgpu,ptxinfo -Mcuda=kepler,maxrregcount:128 -I.    -Minfo=accel -Mnostack_arrays -DUSE_ASYNC 
#OPT=$(OPT_omp)
#OPT=$(OPT_acc)
OPT=$(OPT_acc)

CPP=ftn -E -Mcpp=nocomment $(DFLAGS) $(OPT)

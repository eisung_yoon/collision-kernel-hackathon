! SOLVERLU - SUPER LU, Otherwise - PETSC
!
! Supportint mode
!  - Single species
!  - NOT support Multi species (to be supported?)
!
! option flags
! sml_symmetirc_f = .false.
!
! In XGC1 : collisionf is called by main>f_source>f_collision (col_mode==4)
!subroutine f_collision_single_sp_body(node_in, grid, st, df, col_f_mat,   &
!                                      col_f_ksp,col_f_vecb,col_f_vecx)
! * Difference between the XGC1 collision solver and this standalone version
! 1. Only use of grids (No particle to mesh and mesh to particle interpolation routines)
! 2. Initial distribution is based on anisotropic temperature using an analytic function
! 3. XGC1 version converts mu(prop. to v^2) to v grid while this version only uses v grid

! THE FLOW of standalone version follows f_collision_single_sp_body in collisionf.F90
program collisionf_standalone
  use assert_mod
  use col_sa_fpl_module
  use col_f_mod
  implicit none
  real(kind=8) :: Ti_par, Ti_perp, Ti_avg
  real(kind=8) :: Te_par, Te_perp, Te_avg
  real(kind=8) :: T_avg, v_par, v_perp
  real(kind=8) :: sml_massi_au, massi, dens
  real(kind=8) :: sml_masse_au, masse
  real(kind=8), allocatable, dimension(:,:,:) :: dist_soln_i, dist_soln_e
  real(kind=8), allocatable, dimension(:,:) :: fi, fe
  real (kind=8), allocatable, dimension(:,:,:,:) :: M_ie, M_ei
  !DIR$ ATTRIBUTES FASTMEM :: M_ie, M_ei 
  real(kind=8) :: gammac(4)
  integer :: vpic_ierr
  real(kind=8) :: t_now=0D0 
  integer :: i,j, doindx, mythread
  integer :: istat,icell
  logical :: isok
  character (len=256) :: fname

  integer(8) :: t1,t2,count_rate
  real(kind=8) :: time_col
  integer :: nthreads
!$ integer, external :: omp_get_num_threads
!$ integer, external :: omp_get_thread_num

#ifndef SOLVERLU
  call petsc_init(vpic_ierr)
  if(vpic_ierr .ne. 0) then
    print *, 'PETSc initilization error!'
    stop
  endif
#endif

  nthreads = 1
!$omp parallel
!$omp master
!$    nthreads = omp_get_num_threads()
!$omp end master
!$omp end parallel
  print*,'nthreads = ', nthreads

#ifdef _OPENACC
  print*,'OpenACC recognized'
#endif


  !! : Time stepping
  col_f_dt = 1D-2   !THIS VALUE SHOULD BE DESCRIBED IN COLLISION TIME UNIT (unit:sec now)
  col_f_t_tot = 1   !(unit: sec now)

  call init_col_sa_fpl_module

  !! %USER INPUT FOR DOMAIN OF VELOCITY SPACE%
  !! : Temperature  (unit : eV)
  Ti_par  = 300D0   !300 
  Ti_perp = 500D0   !500
  Te_par  = 400D0   !400
  Te_perp  = 600D0  !600
  !! :  Velocity  (unit : thermal velocity)
  v_par  = 5D0  ! domain range for parallel: [-v_par, v_par]
  v_perp = 5D0  ! domain range for perpendicular: [0, v_perp]
  !! : Mass & Density
  sml_massi_au = 2D0
  sml_masse_au = 1D0/1836D0
  dens = 3D18
  !! %PARAMETER SETTINGS BASED ON USER INPUT%
  !! : avg. Temperature
  Ti_avg = (Ti_par + 2D0 * Ti_perp) / 3D0
  Te_avg = (Te_par + 2D0 * Te_perp) / 3D0
  T_avg  = (Ti_avg + Te_avg) / 2D0
  !! : Mass for colliding and target particles (only ion-ion now)
  massi = sml_massi_au * sml_prot_mass
  masse = sml_masse_au * sml_prot_mass
  !! - Thermal velocity
  vthi_par = sqrt(Ti_par * sml_e_charge / massi)
  vthi_perp = sqrt(Ti_perp * sml_e_charge / massi)
  vthe_par = sqrt(Te_par * sml_e_charge / masse)
  vthe_perp = sqrt(Te_perp * sml_e_charge / masse)
  !! : Domain boundary & Mesh
  vi_par_beg   = -vthi_par*v_par
  vi_par_delta = 2D0*vthi_par*v_par/(col_f_nvz-1)
  vi_perp_delta= vthi_perp*v_perp/(col_f_nvr-1)
  ve_par_beg   = -vthe_par*v_par
  ve_par_delta = 2D0*vthe_par*v_par/(col_f_nvz-1)
  ve_perp_delta= vthe_perp*v_perp/(col_f_nvr-1)
  ! : Domain volume  : ES : NEED TO CHECK IF THIS WAS REALLY PHASE SPACE VOLUME, Note 2 pi factor??

  voli(1) = sml_pi * (1D0/3D0)*vi_perp_delta*vi_perp_delta * vi_par_delta
  !voli(1) = sml_pi * (1D0/4D0)*vi_perp_delta*vi_perp_delta * vi_par_delta
  do i=2, col_f_nvr
     voli(i) = 2D0 * sml_pi * (i-1D0)*vi_perp_delta  * vi_perp_delta * vi_par_delta
  enddo

  if (col_f_multisp_on) then
    !vole(1) = sml_pi * (1D0/4D0)*ve_perp_delta*ve_perp_delta * ve_par_delta
    vole(1) = sml_pi * (1D0/3D0)*ve_perp_delta*ve_perp_delta * ve_par_delta
    do i=2, col_f_nvr
       vole(i) = 2D0 * sml_pi * (i-1D0)*ve_perp_delta  * ve_perp_delta * ve_par_delta
    enddo
  endif

  call col_f_setup

  !! %DYNAMIC MEMORY ALLOCATION%
  !! col_f_nvr x col_f_nvz   
  allocate(dist_soln_i(col_f_nvr, col_f_nvz,ncells), fi(col_f_nvr,col_f_nvz),stat=istat)
  isok = (istat.eq.0)
  call assert(isok,'allocate(dist_soln_i),size=',col_f_nvr*col_f_nvz*ncells)

  if (col_f_multisp_on) then
    allocate(dist_soln_e(col_f_nvr, col_f_nvz,ncells), fe(col_f_nvr,col_f_nvz),stat=istat)
    isok = (istat.eq.0)
    call assert(isok,'allocate(dist_soln_e),size=',col_f_nvr*col_f_nvz*ncells)
  endif

  allocate(M_ie((col_f_nvr-1)*(col_f_nvz-1),3,(col_f_nvr-1)*(col_f_nvz-1),0:nthreads-1), &
           M_ei((col_f_nvr-1)*(col_f_nvz-1),3,(col_f_nvr-1)*(col_f_nvz-1),0:nthreads-1), stat=istat)
  isok = (istat.eq.0)
  call assert(isok,'allocate(M_ie, M_ei),size=',(col_f_nvr-1)*(col_f_nvz-1)*5*(col_f_nvr-1)*(col_f_nvz-1)*nthreads )

  ! PDF initialization
  call make_initial_pdf_bimaxwell(dens,                                 &
                    vi_par_beg, vi_par_delta, vi_perp_delta,             &
                    vthi_par, vthi_perp, fi)
  if (col_f_multisp_on) then
    call make_initial_pdf_bimaxwell(dens,                                 &
                      ve_par_beg, ve_par_delta, ve_perp_delta,             &
                      vthe_par, vthe_perp, fe)
  endif

!$omp parallel do private(icell)
  do icell=lbound(dist_soln_i,3),ubound(dist_soln_i,3)
    dist_soln_i(:,:,icell) = fi(:,:)
  enddo

  if (col_f_multisp_on) then
!$omp parallel do private(icell)
    do icell=lbound(dist_soln_e,3),ubound(dist_soln_e,3)
      dist_soln_e(:,:,icell) = fe(:,:)
    enddo
  endif


  ! get lambda gamma
  call col_f_lambda_gamma(dens,Ti_avg,Te_avg,massi,masse,gammac)

  ! Prepare output
  open(unit=1998,file='ion_moments.dat',status='replace')
  write(1998,*) '# Density (m^(-3)) --- Temperature (eV) --- perp. Temperature (eV)  par. Temperature (eV) --- Momentum (kg m/s) -- Entropy'
  if (col_f_multisp_on) then
    open(unit=1999,file='elec_moments.dat',status='replace')
    write(1999,*) '# Density (m^(-3)) --- Temperature (eV) --- perp. Temperature (eV) par. Temperature (eV) --- Momentum (kg m/s) -- Entropy'
  endif


  time_col = 0

  doindx = 0
  do while(t_now .lt. col_f_t_tot) 
     !! perform collision
     call system_clock(t1,count_rate)


     print *, 't_now = ', t_now
     if (col_f_multisp_on) then
!$omp  parallel do default(none)                                          &
!$omp& shared(dist_soln_i,massi,vi_par_beg,vi_perp_delta,vi_par_delta)    &
!$omp& shared(dist_soln_e,masse,ve_par_beg,ve_perp_delta,ve_par_delta)    &
!$omp& shared(gammac,M_ei,M_ie)                                           &
!$omp& private(icell,vpic_ierr,mythread)
       do icell=lbound(dist_soln_i,3),ubound(dist_soln_i,3)
         mythread = 0
!$       mythread = omp_get_thread_num()
         call col_f_core_m(massi, vi_par_beg, vi_perp_delta, vi_par_delta, dist_soln_i,  &
                           masse, ve_par_beg, ve_perp_delta, ve_par_delta, dist_soln_e,  &
                           icell, gammac,M_ei(1,1,1,mythread),M_ie(1,1,1,mythread), vpic_ierr)
       enddo
     else
!$omp  parallel do default(none)                                          &
!$omp& shared(dist_soln_i,massi,vi_par_beg,vi_perp_delta,vi_par_delta)      &
!$omp& shared(gammac)                                                     &
!$omp& private(icell,vpic_ierr)
       do icell=lbound(dist_soln_i,3),ubound(dist_soln_i,3)
         call col_f_core_s(massi, vi_par_beg, vi_perp_delta, vi_par_delta,  &
                           icell,dist_soln_i,                                 &
                           gammac(1), vpic_ierr)
       enddo
     endif
#ifdef _OPENACC
!$acc  wait
#endif
     call system_clock(t2,count_rate)
     time_col = time_col + dble(t2-t1)/dble(count_rate)

     t_now = t_now + col_f_dt
     doindx = doindx+1

     fi(:,:) = dist_soln_i(:,:,ubound(dist_soln_i,3))
     if (col_f_multisp_on) then
       fe(:,:) = dist_soln_e(:,:,ubound(dist_soln_e,3))
     endif
     write(fname,"(A5,I4.4,A4)") "data_",doindx,"_i.dat"
     open(123,file=fname)
     do i=1, col_f_nvz
        do j=1, col_f_nvr
           write(123,9010) j,i,fi(j,i)
 9010      format('fi(',i6,',',i6,') = ',1pe16.6,';')
        enddo
     enddo
     close(123)

     if (col_f_multisp_on) then
       write(fname,"(A5,I4.4,A4)") "data_",doindx,"_e.dat"
       open(124,file=fname)
       do i=1, col_f_nvz
          do j=1, col_f_nvr
             write(124,9011) j,i,fe(j,i)
 9011      format('fe(',i6,',',i6,') = ',1pe16.6,';')
          enddo
       enddo
       close(124)
     endif


!     print *, '#',doindx,':','t_now =', t_now, 'target t=', col_f_t_tot
     print 9005, doindx,t_now,col_f_t_tot
    print*,'partial time for col_f_core_s is ',time_col,'sec'
 9005 format('#',i6,':','t_now =',1pe14.4,' target t=',1pe14.4)
  enddo

    print*,'total time for col_f_core_s is ',time_col,'sec'

  ! <DYNAMIC MEMOERY DEALLOCATION>
  ! col_f_nvr x col_f_nvz
  deallocate(fi)
  if (col_f_multisp_on) then
     deallocate(fe)
     deallocate(M_ei, M_ie)
  endif   

  close(1998)
  if (col_f_multisp_on) then
    close(1999)
  endif

#ifndef SOLVERLU
  call petsc_end(vpic_ierr)
#endif

end program collisionf_standalone

